from setuptools import setup, Command
import subprocess, os

class BuildAssets (Command):
	user_options = []

	def initialize_options (self):
		pass

	def finalize_options (self):
		pass

	def run (self):
		print ('running')
		try:
			os.makedirs ('src/paperinfod/icons')
		except OSError:
			pass
		ret = subprocess.run (['inkscape', '-S', 'src/assets/weather.svg'], stdout=subprocess.PIPE)
		for line in ret.stdout.decode ('utf-8').split ('\n'):
			try:
				obj, _ = line.split (',', 1)
			except ValueError:
				continue
			if obj.startswith ('layer:'):
				layer = obj[6:]
				print (layer)
				subprocess.run (['inkscape', '-C', '-i', obj, '--export-png-color-mode=RGBA_8', '-o', f'src/paperinfod/icons/{layer}.png', '--export-overwrite', '--export-type=png', 'src/assets/weather.svg', '-j'])

setup(
	name='paperinfod',
	version='0.1',
	author='Lars-Dominik Braun',
	author_email='lars@6xq.net',
	url='',
	packages=['paperinfod'],
	package_dir={'': 'src/'},
	description='',
	long_description=open('README.rst').read(),
	long_description_content_type='text/x-rst',
	install_requires=[
		'astral',
		'pytz',
		'icalendar',
	],
	python_requires='>=3.7',
	cmdclass={'assets': BuildAssets},
	include_package_data=True,
	classifiers = [
		'Development Status :: 4 - Beta',
		'Operating System :: POSIX :: Linux',
		'Programming Language :: Python :: 3',
		],
)
