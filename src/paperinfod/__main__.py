# Copyright (C) 2023 paperinfod contributors
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import locale
locale.setlocale(locale.LC_ALL, 'de_DE.utf8')

import os, struct, fcntl, subprocess, time, traceback, argparse, random, logging, configparser
#from pprint import pprint
from collections import defaultdict
from datetime import datetime, timedelta, time as dtime
from functools import cache
from importlib.resources import files as importfiles, as_file

from pytz import timezone
from astral import LocationInfo
from astral.sun import sun as _sun

from .cairo import Context, ImageSurface, TextExtents, ft_font_face_create_for_ft_face, FontExtents, FontOptions
from .freetype import Context as FTContext
from .ioc import _IOW
from .datasource import DataSource

city = None
localtimezone = None

@cache
def pngToSurface (path):
	""" Cache to avoid expensive disk access """
	return ImageSurface.from_png (path)

@cache
def sun (date):
	""" Cache expensive computation """
	return _sun (city.observer, date)

def drawPng (ctx, path, x, y, width):
	""" Draw from a PNG """

	icon = pngToSurface (path)
	ctx.set_source_surface (icon, x+width/2-icon.width/2, y)
	ctx.paint (ctx)
	return icon.width, icon.height

def drawText (ctx, t, width, x, y, center=False):
	""" Draw centered text t within box of width at x/y """

	extents = FontExtents ()
	ctx.font_extents (extents)
	y += extents.ascent
	height = extents.ascent+extents.descent

	extents = TextExtents ()
	ctx.text_extents (t.encode ('utf8'), extents)
	if center:
		ctx.move_to (x+width/2-extents.width/2, y)
	else:
		ctx.move_to (x, y)
	ctx.show_text (t.encode ('utf8'))

	return extents.width, height

def drawHLine (ctx, width, y):
	""" Draw a horizontal line across the entire context and add padding before/after. """

	y += 25
	ctx.set_source_rgba (0.5, 0.5, 0.5, 1)
	ctx.move_to (0, y)
	ctx.line_to (width, y)
	ctx.stroke (ctx)
	y += 25
	return y

def drawWeather (ctx, top, icon, bottom, isDay, x, y, cellWidth):
	""" Draw a weather icon with text above and below """

	ystart = y

	ctx.set_source_rgba (0, 0, 0, 1)
	ctx.set_font_size (32)
	y += drawText (ctx, top, cellWidth, x, y, center=True)[1]
	y += 10

	for k in ('sun', 'rain', 'snow', 'wind', 'thunderstorm', 'fog', 'finedust'):
		lvl = icon.get (k, None)
		if lvl is None:
			continue
		nightpostfix = '-night' if not isDay else ''
		candidates = [importfiles('paperinfod').joinpath(f'icons/{k}-{lvl}{nightpostfix}.png'),
				importfiles('paperinfod').joinpath(f'icons/{k}-{lvl}.png')]
		for c in candidates:
			with as_file (c) as path:
				if path.exists ():
					iconwidth, _ = drawPng (ctx, str (path), x, y, cellWidth)
					break
	y += iconwidth
	y += 10

	ctx.set_font_size (48)
	ctx.set_source_rgba (0, 0, 0, 1)
	y += drawText (ctx, bottom, cellWidth, x, y, center=True)[1]
	return y-ystart

def draw (framebuffer, datasources, surface, ctx, now):
	today = now.replace (hour=0, minute=0, second=0, microsecond=0)
	tomorrow = (now+timedelta (days=1)).replace (hour=0, minute=0, second=0, microsecond=0)

	dwdforecastData = datasources['dwdforecast'].data['data']
	dwdissuetime = datasources['dwdforecast'].data['issueTime'].astimezone (localtimezone)
	pm25 = datasources['rlpzimen'].data

	data = defaultdict (dict)
	keys = [('R101', 'rain1mm', timedelta(hours=-1)),
			('SunD1', 'sunsec', timedelta(hours=-1)),
			('wwM', 'fog%', timedelta(hours=-1)),
			('wwT', 'thunderstorm%', timedelta(hours=-1)),
			('FF', 'windm/s', timedelta(hours=-1)),
			('FX1', 'windgustm/s', timedelta(hours=-1)),
			('Neff', 'cloudcover%', timedelta(hours=-1)),
			('wwZ', 'drizzle%', timedelta(hours=-1)),
			('DRR1', 'rainsec', timedelta(hours=-1)),
			('wwL', 'rain%', timedelta(hours=-1)),
			('wwS', 'snow%', timedelta(hours=-1)),
			('RR1o1', 'rain10mm', timedelta(hours=-1)),
			('TTT', 'temp', timedelta(hours=0)),
			('Td', 'dewpoint', timedelta (hours=0)),
			]
	for (source, dest, offset) in keys:
		for date, v in dwdforecastData[source]:
			data[date+offset][dest] = [v]
	tmp = []
	# Fill in computed values.
	for k, v in data.items ():
		k = k.astimezone (localtimezone)
		v['start'] = k
		v['end'] = k+timedelta (hours=1)
		s = sun (date=k.date ())
		if s['sunrise'] <= k <= s['sunset']:
			possibleSunSeconds = min (s['sunset'] - k, v['end']-v['start'])
		else:
			possibleSunSeconds = timedelta (seconds=0)
		possibleSunSeconds = int (possibleSunSeconds.total_seconds ())
		v['possibleSunSeconds'] = [possibleSunSeconds]
		isDay = possibleSunSeconds > (v['end']-v['start']).total_seconds ()/2
		v['isDay'] = isDay
		#logging.debug (v['start'], possibleSunSeconds, isDay, s['sunset']-k)
		tmp.append (v)
	data = sorted (tmp, key=lambda x: x['start'])
	#pprint (data)

	def makeIcon (data):
		dimensions = dict ()

		isDay = data.get ('isDay', False)

		if max (data.get ('thunderstorm%', [0])) > 50:
			dimensions['thunderstorm'] = 1
		else:
			dimensions['thunderstorm'] = 0

		windms = max (data.get ('windm/s', [0]))
		windgustms = max (data.get ('windgustm/s', [0]))
		if windms >= 11 or windgustms >= 21:
			# Beauford 6 or 9
			dimensions['wind'] = 2
		elif windms >= 8 or windgustms >= 11:
			# Beauford 5 or 6
			dimensions['wind'] = 1
		else:
			dimensions['wind'] = 0

		fogpct = max (data.get ('fog%', [0]))
		if fogpct > 50:
			dimensions['fog'] = 1
		else:
			dimensions['fog'] = 0

		cloudcoverpct = max (data.get ('cloudcover%', [0]))
		sunsec = sum (data.get ('sunsec', [0]))
		possibleSunSeconds = sum (data.get ('possibleSunSeconds', [0]))
		if cloudcoverpct > 75 or (isDay and sunsec < possibleSunSeconds/10):
			# Mostly cloudy.
			dimensions['sun'] = 0
		elif cloudcoverpct < 50 or (isDay and sunsec > possibleSunSeconds/4*3):
			# Mostly sunny.
			dimensions['sun'] = 2
		else:
			# Mixed.
			dimensions['sun'] = 1

		rainsec = sum (data.get ('rainsec', [0]))
		rain10mmpct = max (data.get('rain10mm', [0]))
		drizzlepct = max (data.get ('drizzle%', [0]))
		rainpct = max (data.get ('rain%', [0]))
		if rainsec > (data['end']-data['start']).total_seconds()/2 or rain10mmpct > 40:
			# Rainy day: >10mm/h
			# see https://www.dwd.de/DE/service/lexikon/Functions/glossar.html?lv2=101812&lv3=101906
			dimensions['rain'] = 2
		elif drizzlepct > 85 or rainpct > 40:
			dimensions['rain'] = 1
		else:
			dimensions['rain'] = 0
		snowpct = max (data.get ('snow%', [0]))
		if snowpct > 25:
			if snowpct > rainpct:
				dimensions['snow'] = 2
				dimensions['rain'] = 0
			else:
				dimensions['snow'] = 1
			print ('let it snow', snowpct)

		return dimensions

	def combineData (data, test, limit):
		""" Combine data based on test function, which returns True if data
		should *not* be merged and a new data point established. Limit results
		to limit number of items. """

		i = 0
		currentData = dict ()
		for d in data:
			if test (currentData, d):
				# flush
				yield currentData
				i += 1
				if i >= limit:
					break
				currentData = dict ()

			for k, v in d.items ():
				if isinstance (v, list):
					vlist = currentData.get (k, [])
					vlist.extend (v)
					currentData[k] = vlist
				else:
					# Only overwrite if not existent yet or special keys
					if k not in currentData or k == 'end':
						currentData[k] = v

	def isDaytime (currentData, d):
		hour = d['start'].hour
		return currentData and 6 <= hour <= 23

	hourlyData = []
	if pm25:
		latestpm25 = list (sorted (pm25, key=lambda x: x[0]))[-1]
		hourlyData.append ((f"{latestpm25[0].hour}", {'finedust': 1}, f"{latestpm25[1]}", True))
	for d in combineData (filter (lambda d: d['end'] >= now, data), isDaytime, 9):
		if d['end'] - d['start'] > timedelta (hours=1):
			top = f"{d['start'].hour}–{d['end'].hour-1}"
		else:
			top = f"{d['start'].hour}"
		# Use minimum, because it’ll be night.
		bottom = f"{round(min (d['temp'])):.0f}°"
		hourlyData.append ((top, makeIcon (d), bottom, d['isDay']))

	def isDaylightChange (currentData, d):
		return currentData and d['isDay'] != currentData.get ('isDay')

	dailyData = []
	currentData = dict ()
	for d in combineData (data, isDaylightChange, 8):
		#logging.debug (d['start'].date (), today)
		if d['start'].date () == today.date ():
			if d['isDay']:
				top = 'heute'
			else:
				top = 'heute Nacht'
		elif d['start'].date () == tomorrow.date ():
			if d['isDay']:
				top = 'morgen'
			else:
				top = 'morgen Nacht'
		elif d['start'].date () != d['end'].date ():
			top = f"{d['start'].strftime('%a')}/{d['end'].strftime('%a')}"
		else:
			top = f"{d['start'].strftime('%a')}"
		#bottom = f"{round(min (d['temp'])):.0f}/{round(max (d['temp'])):.0f}°"
		if 'temp' not in d:
			bottom = f"?°"
		elif d['isDay']:
			bottom = f"{round(max (d['temp'])):.0f}°"
		else:
			bottom = f"{round(min (d['temp'])):.0f}°"
		dailyData.append ((top, makeIcon (d), bottom, d['isDay']))

	padding = 10
	width = framebuffer.width
	height = framebuffer.height
	paddedWidth = width-2*padding

	ctx.save ()

	# Clear background.
	ctx.rectangle (0, 0, width, height)
	ctx.set_source_rgba (1, 1, 1, 1)
	ctx.fill ()

	# Skip padding
	ctx.translate (framebuffer.left, framebuffer.upper)
	# Wiggle image, because partial updates don’t play nicely with static
	# content (leaves blurry edges).
	maxWiggle = 15
	wiggleX = random.randint (0, maxWiggle)
	wiggleY = random.randint (0, maxWiggle)
	ctx.translate (wiggleX, wiggleY)
	height -= maxWiggle
	width -= maxWiggle

	y = padding

	ctx.set_font_size (64)
	ctx.set_source_rgba (0, 0, 0, 1)
	t = now.strftime ('%A, %d. %B %Y · KW %W')
	y += drawText (ctx, t, width, 0, y, center=True)[1]
	y = drawHLine (ctx, width, y)

	# Hourly data.
	cellWidth = paddedWidth/len (hourlyData)
	rowHeight = 0
	for i, (top, icon, bottom, isDay) in enumerate (hourlyData):
		x = padding+i*cellWidth
		#logging.debug (top, icon, bottom, isDay)
		rowHeight = drawWeather (ctx, top, icon, bottom, isDay, x, y, cellWidth)
	y += rowHeight

	y = drawHLine (ctx, width, y)

	# Daily data.
	cellWidth = paddedWidth/len (dailyData)
	rowHeight = 0
	for i, (top, icon, bottom, isDay) in enumerate (dailyData):
		x = padding+i*cellWidth
		#logging.debug (top, icon, bottom, isDay)
		rowHeight = drawWeather (ctx, top, icon, bottom, isDay, x, y, cellWidth)
	y += rowHeight

	y = drawHLine (ctx, width, y)

	x = padding

	# Draw appointments.
	rows = []
	if datasources['calendar'].data:
		for start, end, summary in sorted (datasources['calendar'].data, key=lambda x: x[0]):
			# Skip events in the past
			if end <= now:
				continue
			if start.date() == today.date():
				day = 'heute'
			elif start.date() == tomorrow.date():
				day = 'morgen'
			elif start-now < timedelta (days=7):
				day = f'{start.strftime ("%a")}'
			else:
				day = f'{start.strftime ("%d.%m.")}'
			if start.time() != dtime ():
				time = f'{start.strftime("%H:%M")}'
			else:
				time = ''
			rows.append ((day, time, summary))

	if rows:
		ctx.set_font_size (48)
		ctx.set_source_rgba (0, 0, 0, 1)
		x = padding+40
		for column in range (len (rows[0])):
			maxColWidth = 0
			yoff = 0
			for r in rows:
				w, h = drawText (ctx, r[column], paddedWidth/2, x, y+yoff)
				maxColWidth = max (w, maxColWidth)
				yoff += h
				if yoff > 5*48:
					break
			x += maxColWidth+40

	# Status bar
	battery = datasources["battery"].data
	t = []
	if battery is not None:
		t.append (f'Batterie {datasources["battery"].data}%')
	t.append (f'Vorhersage {dwdissuetime.strftime("%H").lstrip ("0")} Uhr')
	t.append (f'Aktualisierung {now.strftime("%H:%M")}')
	t = ' · '.join (t)
	ctx.set_source_rgba (0, 0, 0, 1)
	ctx.set_font_size (24)
	extents = TextExtents ()
	ctx.text_extents (t.encode ('utf8'), extents)
	y += extents.height
	ctx.move_to (width-extents.width-padding, height-extents.height)
	ctx.show_text (t.encode ('utf8'))

	ctx.restore ()

	framebuffer.write (surface)

def writeSysfs (key, data):
	with open (f'/sys/{key}', 'w') as fd:
		fd.write (data)

def sleepUntil (d, soft=False):
	logging.info (f'sleeping until {d}')
	while True:
		try:
			now = datetime.now(localtimezone)
			if now >= d:
				break
			logging.info ('going to sleep')
			if isKindle ():
				writeSysfs ('class/rtc/rtc1/wakealarm', str (d.timestamp ()))
			if soft or not isKindle ():
				time.sleep (10)
			else:
				# lab126’s powerd also uses “mem”.
				writeSysfs ('power/state', 'mem')
				time.sleep (2)
			logging.info ('woke up')
		except Exception as e:
			# Fall back to soft variant
			logging.error (f'failed {e}')
			# Sleep does not take hibernation into account, so we cannot sleep for the full duration.
			time.sleep (10)

_isKindle = None
def isKindle ():
	global _isKindle
	if _isKindle is None:
		with open ('/proc/cpuinfo') as fd:
			_isKindle = 'Freescale i.MX 6SoloLite based Wario Board' in fd.read ()
	return _isKindle

class Framebuffer:
	def write (self, surface):
		raise NotImplementedError ()

class PngFramebuffer (Framebuffer):
	def __init__ (self, width, height, path):
		self.width = width
		self.height = height
		self.bufferWidth = width
		self.bufferHeight = height
		self.left = 0
		self.right = 0
		self.upper = 0
		self.lower = 0
		self.path = path

	def __repr__ (self):
		return f'<{self.__class__.__name__} path={self.path}>'

	def write (self, surface):
		ret = surface.write_to_png (self.path)
		if ret != 0:
			logging.error (f'Failed to write PNG framebuffer to {self.path}: {ret}')

class KindleFramebuffer (Framebuffer):
	def __init__ (self, path, maxGhostingWrites=10):
		self.width = 1448
		self.height = 1078
		self.bufferWidth = 1472
		self.bufferHeight = 4608
		# These apply to bufferWidth and bufferHeight
		self.left = 0 # was: 16
		self.right = 104
		self.upper = 4
		self.lower = 4
		self.fd = os.open (path, os.O_RDWR)
		self.lastMarker = 0
		# eInk display should be refreshed entirely now and then to get rid of
		# ghosting/blurriness. But not too often, because a full refresh
		# (presumably) uses more energy.
		self.maxGhostingWrites = maxGhostingWrites
		# First update should always clear the entire screen (fresh start).
		self.ghostingWriteCount = maxGhostingWrites

	def __del__ (self):
		os.close (self.fd)

	def __repr__ (self):
		return f'<{self.__class__.__name__} fd={self.fd} lastMarker={self.lastMarker}>'

	def blank (self, enable=True):
		FB_BLANK_UNBLANK = 0
		FB_BLANK_POWERDOWN = 4
		FBIOBLANK = 0x4611
		fcntl.ioctl (self.fd, FBIOBLANK, FB_BLANK_UNBLANK if not enable else FB_BLANK_POWERDOWN)

	def sendUpdate (self, clearGhosting=False):
		WAVEFORM_MODE_DU = 0x1
		WAVEFORM_MODE_GC16 = 0x2
		WAVEFORM_MODE_GC16_FAST = 0x3
		WAVEFORM_MODE_REAGL = 0x8
		UPDATE_MODE_PARTIAL = 0x0
		UPDATE_MODE_FULL = 0x1
		TEMP_USE_AUTO = 0x1001
		flags = 0
		fmt = 'IIIIIIIIIiIIIIIIII'
		waitForUpdateCompleteFmt = 'II'
		MXCFB_SEND_UPDATE = _IOW(ord('F'), 0x2e, struct.calcsize (fmt))
		MXCFB_WAIT_FOR_UPDATE_COMPLETE = _IOW(ord('F'), 0x2f, struct.calcsize (waitForUpdateCompleteFmt))

		self.lastMarker += 1
		update = struct.pack (fmt,
				0,
				0,
				self.width,
				self.height-8, # XXX: no idea why
				WAVEFORM_MODE_REAGL if not clearGhosting else WAVEFORM_MODE_GC16,
				UPDATE_MODE_FULL, # We always write the entire framebuffer
				self.lastMarker,
				WAVEFORM_MODE_REAGL if not clearGhosting else WAVEFORM_MODE_GC16,
				WAVEFORM_MODE_REAGL if not clearGhosting else WAVEFORM_MODE_GC16,
				TEMP_USE_AUTO,
				flags,
				0,
				0,
				0,
				0,
				0,
				0,
				0)
		fcntl.ioctl (self.fd, MXCFB_SEND_UPDATE, update)

	def write (self, surface):
		data = surface.data
		# Convert to grayscale (using only red channel)
		data = bytes (data[0:(self.bufferWidth*self.bufferHeight)*4:4])
		os.lseek (self.fd, 0, os.SEEK_SET)
		os.write (self.fd, data)

		self.blank (False)
		if self.ghostingWriteCount < self.maxGhostingWrites:
			self.sendUpdate (clearGhosting=False)
			self.ghostingWriteCount += 1
		else:
			self.sendUpdate (clearGhosting=True)
			self.ghostingWriteCount = 0
		# Blanking will also wait for the update to finish, according to the kernel sources.
		self.blank (True)

		# No collisions possible, we’re the only writer.
		# XXX: does not work
		#data = struct.pack (waitForUpdateCompleteFmt, self.lastMarker, 0)
		#fcntl.ioctl (self.fd, MXCFB_WAIT_FOR_UPDATE_COMPLETE, data)

def wpaStatus ():
	logging.debug ('Fetching wpa_supplicant status')
	proc = subprocess.run (['wpa_cli', 'status'], capture_output=True)
	status = {}
	for l in proc.stdout.decode('utf8').split ('\n'):
		try:
			k, v = l.split ('=', 1)
			status[k] = v
		except ValueError:
			pass
	logging.debug (f'Status is {status}')
	return status

def initDatasources (config):
	sourceClasses = dict (map (lambda c: (c.name, c), DataSource.__subclasses__ ()))
	datasources = {}
	for s in config.sections ():
		if s.startswith ('datasource.'):
			_, source = s.split ('.', 1)
			datasources[source] = sourceClasses[source] (**config[s])
	return datasources

def main ():
	global city, localtimezone

	parser = argparse.ArgumentParser(description='Kindle eInk information display')
	parser.add_argument ('--debug', action='store_true')
	parser.add_argument ('configFile', metavar='FILE')
	args = parser.parse_args()

	logging.basicConfig (format='%(asctime)s %(levelname)s %(message)s',
			level=logging.DEBUG if args.debug else logging.INFO)

	config = configparser.ConfigParser()
	config.read(args.configFile)

	city = LocationInfo("", "", config.get ('time', 'zone'), config.get ('time', 'latitude'), config.get ('time', 'longitude'))
	localtimezone = timezone (config.get ('time', 'zone'))

	datasources = initDatasources (config)
	logging.info (f'Configured datasources {datasources}')

	if isKindle ():
		framebuffer = KindleFramebuffer ('/dev/fb0')
	else:
		framebuffer = PngFramebuffer (1448, 1072, 'display.png')
	logging.info (f'Using framebuffer {framebuffer}')

	# Init fonts and drawing context.
	ft = FTContext ()
	resource = importfiles('paperinfod').joinpath('font/Lato-Regular.ttf')
	with as_file (resource) as path:
		face = ft.new_face (str (path), 0)
	font = ft_font_face_create_for_ft_face (face, 0)
	surface = ImageSurface.create (ImageSurface.FORMAT_RGB24, framebuffer.bufferWidth, framebuffer.bufferHeight)
	ctx = Context.create (surface)
	ctx.set_font_face (font)
	# Looks weird without antialiasing, even though display has reasonably high dpi.
	opts = FontOptions.create ()
	opts.set_antialias (opts.ANTIALIAS_GRAY)
	opts.set_hint_style (opts.HINT_STYLE_FULL)
	ctx.set_font_options (opts)

	if isKindle ():
		# We don’t need these:
		unusedServices = (
				'framework',
				'otaupd',
				'otav3',
				'x',
				'dpmd',
				'rcm',
				'tmd',
				'todo',
				'phd',
				'archive',
				'appmgrd',
				'powerd',
				'cron',
				'wifid',
				# The wifi module loader. We load it ourselves.
				'wifim',
				'wifis',
				'cmd',
				# Will stop usbnet.
				#'dbus',
				#'volumd',
				'dmld',
				'demd',
				'stackdumpd',
				)
		for service in unusedServices:
			proc = subprocess.run (['initctl', 'stop', service], capture_output=True)
		# Disable fb rotation -> landscape with USB to the right.
		writeSysfs ('devices/platform/imx_epdc_fb/graphics/fb0/rotate', '0')
		# We don’t need to go fast.
		writeSysfs ('devices/system/cpu/cpu0/cpufreq/scaling_governor', 'powersave')
		# Disable backlight to save power.
		writeSysfs ('class/backlight/max77696-bl/brightness', '0')
		# We don’t need the hall sensor.
		writeSysfs ('devices/system/wario_hall/wario_hall0/hall_enable', '0')
		# Disable touchscreen power.
		with open ('/proc/touch', 'w') as fd:
			fd.write ('lock')
		# Make sure SSL certs can be found.
		os.environ['SSL_CERT_FILE'] = '/etc/ssl/certs/ca-certificates.crt'
		# Ensure wifi driver is loaded.
		# See /etc/upstart/wifim.conf
		# suspend_mode=WLAN_POWER_STATE_CUT_PWR
		# recovery_enable seems to poll the hardware regularly to ensure it’s still operational.
		subprocess.run(['modprobe', 'ath6kl_sdio',
				'recovery_enable=1', 'suspend_mode=1',
				'board_data_file=/opt/ar6k/target/AR6003/hw2.1.1/bin/AR6003_wfo_calfile.bin'])
		wpaSupplicantConfig = '/tmp/wpa_supplicant.conf'
		with open (wpaSupplicantConfig, 'w') as fd:
			ssid = config.get('wifi', 'ssid')
			psk = config.get('wifi', 'psk')
			fd.write (f"""ctrl_interface=/var/run/wpa_supplicant
update_config=1
ap_scan=2
device_name=paperinfod

network={{
	ssid="{ssid}"
	key_mgmt=WPA-PSK
	psk={psk}
}}
""")

	while True:
		# Wait until we’re connected to WiFi.
		if isKindle ():
			isConnected = False
			logging.debug ('Starting wpa_supplicant')
			wpaSupplicantProcess = subprocess.Popen (['wpa_supplicant',
					f'-c{wpaSupplicantConfig}',
					'-Dnl80211',
					'-iwlan0'])
			i = 0
			wpaConnected = False
			while True:
				logging.debug ('Waiting for WPA success')
				status = wpaStatus ()
				if status.get('wpa_state') == 'COMPLETED':
					wpaConnected = True
					break
				else:
					# Check if it’s still running
					try:
						ret = wpaSupplicantProcess.wait (0)
						logging.error (f'WPA supplicant died {ret}')
						break
					except subprocess.TimeoutExpired:
						pass
				time.sleep (1)
			if wpaConnected:
				logging.debug ('Starting DHCPc')
				# Allow udhcpc to set the default route.
				try:
					os.unlink ('/tmp/no-default-route')
				except FileNotFoundError:
					pass
				# The kindle’s /etc/resolv.conf is symlinked to /var/run, but udhcpc
				# puts it in /tmp by default.
				try:
					os.symlink ('/tmp/resolv.conf', '/var/run/resolv.conf')
				except FileExistsError:
					pass
				dhcpProcess = subprocess.Popen (['udhcpc', '-n', '-i', 'wlan0', '-f'])
				while True:
					logging.debug ('Waiting for IP address')
					status = wpaStatus ()
					if 'ip_address' in status:
						isConnected = True
						break
					time.sleep (1)
		else:
			isConnected = True

		now = datetime.now (localtimezone)
		if isConnected:
			if isKindle ():
				logging.debug ('Fetching time')
				proc = subprocess.run (['ntpdate', 'de.pool.ntp.org'])

			now = datetime.now (localtimezone)

			logging.info ('fetching data')
			nextRefresh = []
			for s in datasources.values ():
				try:
					r = s.refresh (now)
					logging.debug (f'{s} wants to refresh at {r}')
					if r:
						r = r.astimezone (localtimezone)
						nextRefresh.append (r)
				except Exception:
					traceback.print_exc ()

			if isKindle ():
				dhcpProcess.terminate ()
				wpaSupplicantProcess.terminate ()
				# com.lab126.cmd actually removes the driver. Probably because there’s
				# no rfkill interface. Should not be necessary, as the driver can suspend
				# the card.
				#subprocess.run(['rmmod', 'ath6kl_sdio'])

		logging.info ('redrawing')
		# Bumping up the CPU speed allows us to speed up rendering, so we can
		# go back to sleep faster. Everything else is IO-bound, but this one is
		# actually CPU-bound.
		if isKindle ():
			writeSysfs ('devices/system/cpu/cpu0/cpufreq/scaling_governor', 'ondemand')
		try:
			draw (framebuffer, datasources, surface, ctx, now)
		except Exception:
			traceback.print_exc ()
		if isKindle ():
			writeSysfs ('devices/system/cpu/cpu0/cpufreq/scaling_governor', 'powersave')

		# Do not refresh during night-time (0–6 h) to save energy and eink
		if now.time() >= dtime (hour=23):
			nextRedraw = (now + timedelta (hours=6)).replace (hour=6, minute=0, second=0, microsecond=0)
		elif now.time() < dtime (hour=6):
			nextRedraw = now.replace (hour=6, minute=0, second=0, microsecond=0)
		else:
			# Refresh at least every hour.
			upperLimit = now + timedelta (hours=1, minutes=5)
			# Ignore everything older than now.
			nextRefresh = list (filter (lambda x: now < x < upperLimit, nextRefresh))
			if nextRefresh:
				nextRedraw = max (nextRefresh)
			else:
				nextRedraw = now + timedelta (hours=1)

		sleepUntil (nextRedraw)

main ()

