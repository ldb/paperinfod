# Copyright (C) 2023 paperinfod contributors
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import csv, re, os, logging, urllib, random
import datetime as dtmod
from collections import defaultdict
from datetime import datetime, timedelta
from decimal import Decimal
from io import BytesIO, TextIOWrapper, StringIO
from urllib.request import urlopen
from urllib.parse import urlparse
from xml.etree import ElementTree
from zipfile import ZipFile

from pytz import timezone
from icalendar import Calendar

class DataSource:
	""" Data source base class. """

	# Configuration name.
	name = None

	def __init__ (self):
		self.nextRefresh = None
		self.data = None

	def _refresh (self, now):
		raise NotImplementedError ()

	def refresh (self, now):
		if self.nextRefresh and now < self.nextRefresh:
			return self.nextRefresh
		self.nextRefresh = self._refresh (now)
		return self.nextRefresh

class DwdNow(DataSource):
	""" Current temperature and humidity observations by DWD. """

	name = 'dwdnow'

	def __init__ (self, station):
		super().__init__ ()

		self.url = f'https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/10_minutes/air_temperature/now/10minutenwerte_TU_{station}_now.zip'

	def _refresh (self, now):
		data = None
		logging.debug (f'Fetching {self.url}')
		with urlopen (self.url, timeout=15) as resp:
			# Data is not big, we can buffer in memory
			binaryFile = BytesIO (resp.read())

		with ZipFile (binaryFile) as z:
			for f in z.namelist ():
				with z.open (f) as fd:
					header = None
					for row in csv.reader (TextIOWrapper (fd, encoding='utf-8'), delimiter=';'):
						if header is None:
							header = row
						else:
							d = dict (zip (header, row))
							timestamp = datetime.strptime (d['MESS_DATUM'], '%Y%m%d%H%M')
							timestamp = datetime.combine (timestamp.date(), timestamp.time(), timezone('UTC'))
							data = dict(
									temperature=Decimal (d['TT_10']),
									humidity=Decimal (d['RF_10']),
									dewpoint=Decimal (d['TD_10']), # calculated, not measured.
									pressure=Decimal (d['PP_10']),
									time=timestamp,
									)
		self.data = data

class DwdForecast(DataSource):
	""" DWD forecast data for particular station. """

	name = 'dwdforecast'

	def __init__ (self, station):
		super().__init__ ()

		self.url = f'https://opendata.dwd.de/weather/local_forecasts/mos/MOSMIX_L/single_stations/{station}/kml/MOSMIX_L_LATEST_{station}.kmz'

	def _refresh (self, now):
		data = None

		logging.debug (f'Fetching {self.url}')
		with urlopen (self.url, timeout=15) as resp:
			# Need a seekable file.
			binaryFile = BytesIO (resp.read ())

		with ZipFile (binaryFile) as z:
			for f in z.namelist ():
				with z.open (f) as fd:
					data = fd.read ()
					root = ElementTree.fromstring(data)
					ns = {'kml': 'http://www.opengis.net/kml/2.2',
					'dwd': 'https://opendata.dwd.de/weather/lib/pointforecast_dwd_extension_V1_0.xsd'}
					document = root.find ('kml:Document', ns)
					data = document.find ('kml:ExtendedData', ns)
					definition = data.find ('dwd:ProductDefinition', ns)

					issueTime = definition.find ('dwd:IssueTime', ns)
					issueTime = datetime.fromisoformat (issueTime.text.rstrip ('Z'))
					issueTime = datetime.combine (issueTime.date(), issueTime.time(), timezone ('UTC'))

					timesteps = definition.find ('dwd:ForecastTimeSteps', ns)
					steps = []
					for s in timesteps.findall ('dwd:TimeStep', ns):
						timestamp = datetime.fromisoformat (s.text.rstrip ('Z'))
						timestamp = datetime.combine (timestamp.date(), timestamp.time(), timezone('UTC'))
						steps.append (timestamp)

					data = dict (steps=steps)
					placemark = document.find ('kml:Placemark', ns)
					extendedData = placemark.find ('kml:ExtendedData', ns)
					for s in extendedData.findall ('dwd:Forecast', ns):
						name = s.attrib[f'{{{ns["dwd"]}}}elementName']
						value = s.find ('dwd:value', ns)
						values = [None if x in {'-', ''} else x for x in re.split (r'\s+', value.text)[1:]]
						if name in {'TTT', 'TX', 'TN', 'Td'}:
		    				# Temperature in Kelvin
							values = [Decimal(v)-Decimal('273.15') if v is not None else None for v in values]
						elif name in {'ww', 'ww3', 'WPc11', 'WPc31', 'WPc61', 'WPcd1', 'WPch1', 'wwL', 'wwS', 'wwZ', 'R101', 'R105', 'SunD1', 'DRR1', 'wwM', 'wwT', 'Neff', 'RR1o1'}:
		    				# Integer values
							values = [int (Decimal (v)) if v is not None else None for v in values]
						elif name in {'FX1', 'FF'}:
							values = [Decimal (v) if v is not None else None for v in values]
						assert len (values) == len (steps)
						data[name] = list (sorted (filter (lambda x: x[1] is not None, zip (steps, values)), key=lambda x: x[0]))
						# See https://opendata.dwd.de/weather/lib/MetElementDefinition.xml for units
		self.data = dict (data=data, issueTime=issueTime)

		# Refresh at 3, 9, 15, 21 UTC (although it may take a few minutes)
		return issueTime+timedelta (hours=6, minutes=10)

class RlpZimen(DataSource):
	""" RLP’s Zimen air quality station data. """

	name = 'rlpzimen'

	def __init__ (self, location):
		super().__init__ ()

		self.location = location
		self.url = 'https://listen.rlp.de/fileadmin/zimen/data/data.php?data=297568ba27a344a1e0651b5e00975d6a'

	def _refresh (self, now):
		logging.debug (f'Fetching {self.url}')
		with urlopen (self.url, timeout=15) as resp:
			data = list (csv.reader (StringIO (resp.read().decode ('utf-8')), delimiter=';'))

		headerA = data[0][0]
		headerAPrefix = 'Luftmesswerte - PM2,5 [µg/m³] vom '
		assert headerA.startswith (headerAPrefix)
		date = datetime.strptime (headerA[len(headerAPrefix):], '%d.%m.%Y %H:%M').replace (tzinfo=timezone ('Europe/Berlin'))

		headerC = data[3]
		assert headerC[0] == 'Standorte', headerC

		measureTimes = []
		for x in headerC[1:]:
			x = x.strip ()
			if not x:
				measureTimes.append (None)
				continue

			# 0:00 is actually displayed as 24:00
			if x == '24:00':
				x = '0:00'
				addDate = date+timedelta (days=1)
			else:
				addDate = date
			parsedTime = datetime.strptime (x, '%H:%M')
			dataPointDate = datetime.combine (addDate, parsedTime.time (), addDate.tzinfo)
			measureTimes.append (dataPointDate)

		result = defaultdict (list)
		for d in data[4:]:
			k = d[0]
			for time, value in zip (measureTimes, d[1:]):
				if time and value != '-':
					time = datetime.combine (date, time.time (), date.tzinfo)
					value = int (value)
					result[k].append ((time, value))
		self.data = result.get (self.location, [])

		# Usually refreshed at :10 to :15
		return (date+timedelta (hours=1)).replace (minute=10)

class Battery(DataSource):
	""" Battery gauge. """

	name = 'battery'

	def _refresh (self, now):
		base = '/sys/class/power_supply'
		capacity = 0
		n = 0
		for kind in os.listdir (base):
			path = os.path.join (base, kind, 'capacity')
			if os.path.exists (path):
				with open (path) as fd:
					capacity += int (fd.read ().strip ())
					n += 1
		if n:
			data = capacity//n
		else:
			data = None
		self.data = data

		# Don’t know when it’ll change
		return None

class ICalendar(DataSource):
	""" WebDav calendar entries. """

	name = 'calendar'

	def __init__ (self, url, user, password):
		super().__init__ ()
		self.url = url

		auth_handler = urllib.request.HTTPBasicAuthHandler()
		auth_handler.add_password(realm='Radicale - Password Required',
				uri=urlparse (url)._replace (path='').geturl (),
				user=user, passwd=password)
		opener = urllib.request.build_opener(auth_handler)
		urllib.request.install_opener(opener)

	def _refresh (self, now):
		data = []
		try:
			with urlopen (self.url, timeout=15) as resp:
				cal = Calendar.from_ical(resp.read())
			for component in cal.walk():
				if component.name == 'VEVENT':
					if 'dtstart' in component:
						start = component['dtstart'].dt
						if 'dtend' in component:
							end = component['dtend'].dt
						else:
							end = start

						summary = component['summary']
						if type (start) is datetime:
							# ensure dates are not naive
							if start.tzinfo is None or start.tzinfo.utcoffset(start) is None:
								start = start.replace (tzinfo=timezone('UTC'))
							if end.tzinfo is None or end.tzinfo.utcoffset(end) is None:
								end = end.replace (tzinfo=timezone('UTC'))
							data.append ((start, end, summary))
						elif type (start) is dtmod.date:
							start = datetime.combine (start, dtmod.time (), timezone ('UTC'))
							end = datetime.combine (end, dtmod.time (), timezone ('UTC'))
							data.append ((start, end, summary))
			self.data = data
		except urllib.error.URLError as e:
			logging.error (f'Cannot fetch calendar from {self.url}: {e}')

		# We don’t know when it’ll change.
		return None

