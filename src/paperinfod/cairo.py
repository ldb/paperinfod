# Copyright (C) 2023 paperinfod contributors
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from functools import partial
import os
import ctypes
import ctypes.util
from ctypes import c_void_p, c_double, Structure, c_int, c_char_p, c_ubyte

# Enumeration of 
OPERATOR_CLEAR = 0

OPERATOR_SOURCE = 1
OPERATOR_OVER = 2
OPERATOR_IN = 3
OPERATOR_OUT = 4
OPERATOR_ATOP = 5

OPERATOR_DEST = 6
OPERATOR_DEST_OVER = 7
OPERATOR_DEST_IN = 8
OPERATOR_DEST_OUT = 9
OPERATOR_DEST_ATOP = 10

OPERATOR_XOR = 11
OPERATOR_ADD = 12
OPERATOR_SATURATE = 13

OPERATOR_MULTIPLY = 14
OPERATOR_SCREEN = 15
OPERATOR_OVERLAY = 16
OPERATOR_DARKEN = 17
OPERATOR_LIGHTEN = 18
OPERATOR_COLOR_DODGE = 19
OPERATOR_COLOR_BURN = 20
OPERATOR_HARD_LIGHT = 21
OPERATOR_SOFT_LIGHT = 22
OPERATOR_DIFFERENCE = 23
OPERATOR_EXCLUSION = 24
OPERATOR_HSL_HUE = 25
OPERATOR_HSL_SATURATION = 26
OPERATOR_HSL_COLOR = 27
OPERATOR_HSL_LUMINOSITY = 28

class TextExtents(Structure):
	_fields_ = [("x_bearing", c_double),
				("y_bearing", c_double),
				("width", c_double),
				("height", c_double),
				("x_advance", c_double),
				("y_advance", c_double),
	]

class FontExtents(Structure):
	_fields_ = [("ascent", c_double),
				("descent", c_double),
				("height", c_double),
				("max_x_advance", c_double),
				("max_y_advance", c_double),
	]

# For some reason we have to manually open freetype, otherwise loading cairo fails with
# OSError: /usr/lib/libcairo.so: undefined symbol: FT_Get_Emboldening_Factor
from .freetype import freetype

guixPath = os.getenv ('GUIX_ENVIRONMENT', '/usr')
libPaths = [guixPath + '/lib', '/usr/lib64', '/usr/lib']
for p in libPaths:
	try:
		cairo = ctypes.CDLL(p + '/libcairo.so')
		break
	except OSError:
		pass

c_cairo_t_p = c_void_p
c_surface_t_p = c_void_p
c_cairo_font_options_t_p = c_void_p

class Context:
	__slots__ = ('_ctx', )

	FONT_SLANT_NORMAL = 0
	FONT_SLANT_ITALIC = 1
	FONT_SLANT_OBLIQUE = 2

	FONT_WEIGHT_NORMAL = 0

	_create = cairo.cairo_create
	_create.restype = ctypes.POINTER(c_void_p)
	_create.argtypes = [c_surface_t_p]

	_destroy = cairo.cairo_destroy
	_destroy.argtypes = [c_surface_t_p]

	_save = cairo.cairo_save
	_save.argtypes = [c_surface_t_p]

	_restore = cairo.cairo_restore
	_restore.argtypes = [c_surface_t_p]

	_set_source_surface = cairo.cairo_set_source_surface
	_set_source_surface.argtypes = [c_cairo_t_p, c_surface_t_p, c_double, c_double]

	_rectangle = cairo.cairo_rectangle
	_rectangle.argtypes = [c_cairo_t_p, c_double, c_double, c_double, c_double]

	_set_source_rgba = cairo.cairo_set_source_rgba
	_set_source_rgba.argtypes = [c_void_p, c_double, c_double, c_double, c_double]

	_set_source_rgb = cairo.cairo_set_source_rgb
	_set_source_rgb.argtypes = [c_void_p, c_double, c_double, c_double]

	_set_operator = cairo.cairo_set_operator

	_set_line_width = cairo.cairo_set_line_width
	_set_line_width.argtypes = [c_void_p, c_double]

	_stroke = cairo.cairo_stroke
	_fill = cairo.cairo_fill
	_paint = cairo.cairo_paint

	_move_to = cairo.cairo_move_to
	_move_to.argtypes = [c_void_p, c_double, c_double]

	_line_to = cairo.cairo_line_to
	_line_to.argtypes = [c_void_p, c_double, c_double]

	_curve_to = cairo.cairo_curve_to
	_curve_to.argtypes = [c_void_p, c_double, c_double, c_double, c_double, c_double, c_double]

	_close_path = cairo.cairo_close_path

	_text_extents = cairo.cairo_text_extents
	_text_extents.argtypes = [c_cairo_t_p, c_char_p, ctypes.POINTER(TextExtents)]

	_font_extents = cairo.cairo_font_extents
	_font_extents.argtypes = [c_cairo_t_p, ctypes.POINTER(FontExtents)]

	_show_text = cairo.cairo_show_text
	_show_text.argtypes = [c_cairo_t_p, c_char_p]

	_translate = cairo.cairo_translate
	_translate.argtypes = [c_cairo_t_p, c_double, c_double]

	_rotate = cairo.cairo_rotate
	_rotate.argtypes = [c_cairo_t_p, c_double]

	_select_font_face = cairo.cairo_select_font_face
	_select_font_face.argtypes = [c_cairo_t_p, c_char_p, c_int, c_int]

	_set_font_size = cairo.cairo_set_font_size
	_set_font_size.argtypes = [c_cairo_t_p, c_double]

	_set_font_face = cairo.cairo_set_font_face
	_set_font_face.argtypes = [c_cairo_t_p, c_void_p]

	_set_font_options = cairo.cairo_set_font_options
	_set_font_options.argtypes = [c_cairo_t_p, c_cairo_font_options_t_p]

	def __init__ (self, ctx):
		self._ctx = ctx

	@classmethod
	def create (cls, surface):
		return cls (cls._create (surface))

	@property
	def _as_parameter_ (self):
		return self._ctx

	def select_font_face (self, name, slant, weight):
		return self._select_font_face (self, name.encode ('utf8'), slant, weight)

	def __getattr__ (self, name):
		try:
			f = self.__getattribute__ ('_' + name)
			return partial (f, self)
		except AttributeError as e:
			raise AttributeError () from e

	def __del__ (self):
		self._destroy (self._ctx)

class ImageSurface:
	# Enumerate pixel formats
	FORMAT_INVALID   = -1
	FORMAT_ARGB32	= 0
	FORMAT_RGB24	 = 1
	FORMAT_A8		= 2
	FORMAT_A1		= 3
	FORMAT_RGB16_565 = 4
	#FORMAT_RGB30	 = 5
	# On the Kindle, this is a different thing, but it returns CAIRO_STATUS_INVALID_FORMAT?! Maybe the system libcairo is not built with #define LAB126?
	#FORMAT_E8	 = 5
	#FORMAT_A8E8	 = 6

	_image_surface_create = cairo.cairo_image_surface_create
	_image_surface_create.restype = ctypes.POINTER(c_void_p)

	_image_surface_create_from_png = cairo.cairo_image_surface_create_from_png
	_image_surface_create_from_png.argtypes = [c_char_p]
	_image_surface_create_from_png.restype = ctypes.POINTER(c_void_p)

	_surface_write_to_png = cairo.cairo_surface_write_to_png
	_surface_write_to_png.argtypes = [c_surface_t_p, c_char_p]
	_surface_write_to_png.restype = c_int

	_image_surface_get_data = cairo.cairo_image_surface_get_data
	_image_surface_get_data.restype = ctypes.POINTER(c_ubyte)

	_image_surface_get_width = cairo.cairo_image_surface_get_width
	_image_surface_get_width.argtypes = [c_surface_t_p]
	_image_surface_get_width.restype = c_int

	_image_surface_get_height = cairo.cairo_image_surface_get_height
	_image_surface_get_height.argtypes = [c_surface_t_p]
	_image_surface_get_height.restype = c_int

	_surface_destroy = cairo.cairo_surface_destroy

	_surface_flush = cairo.cairo_surface_flush
	_surface_flush.argtypes = [c_surface_t_p]

	def __init__ (self, surface):
		self._surface = surface

	@classmethod
	def create (cls, format, width, height):
		return cls (cls._image_surface_create (format, width, height))

	@classmethod
	def from_png (cls, path):
		return cls (cls._image_surface_create_from_png (path.encode ('utf-8')))

	@property
	def _as_parameter_ (self):
		return self._surface

	@property
	def width (self):
		return self._image_surface_get_width (self._surface)

	@property
	def height (self):
		return self._image_surface_get_height (self._surface)

	def write_to_png (self, path):
		return self._surface_write_to_png (self, path.encode ('utf-8'))

	def flush (self):
		self._surface_flush (self)

	def save (self):
		self._save (self)

	def restore (self):
		self._restore (self)

	@property
	def data (self):
		self.flush ()
		return self._image_surface_get_data (self)

	def __del__ (self):
		self._surface_destroy (self._surface)

class FontOptions:
	ANTIALIAS_NONE = 1
	ANTIALIAS_GRAY = 2
	HINT_STYLE_NONE = 1
	HINT_STYLE_FULL = 4

	_font_options_create = cairo.cairo_font_options_create
	_font_options_create.restype = ctypes.POINTER(c_void_p)

	_font_options_destroy = cairo.cairo_font_options_destroy
	_font_options_destroy.argtypes = [c_cairo_font_options_t_p]

	_font_options_set_antialias = cairo.cairo_font_options_set_antialias
	_font_options_set_antialias.argtypes = [c_cairo_font_options_t_p, c_int]

	_font_options_set_hint_style = cairo.cairo_font_options_set_hint_style
	_font_options_set_hint_style.argtypes = [c_cairo_font_options_t_p, c_int]

	def __init__ (self, opts):
		self._opts = opts

	def __del__ (self):
		self._font_options_destroy (self._opts)

	@classmethod
	def create (cls):
		return cls (cls._font_options_create ())

	@property
	def _as_parameter_ (self):
		return self._opts

	def set_antialias (self, opt):
		self._font_options_set_antialias (self._opts, opt)

	def set_hint_style (self, opt):
		self._font_options_set_hint_style (self._opts, opt)

status_to_string = cairo.cairo_status_to_string
status_to_string.argtypes = [c_int]
status_to_string.restype = c_char_p


ft_font_face_create_for_ft_face = cairo.cairo_ft_font_face_create_for_ft_face
ft_font_face_create_for_ft_face.argtypes = [c_void_p, c_int]
ft_font_face_create_for_ft_face.restype = c_void_p

