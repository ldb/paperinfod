# Copyright (C) 2023 paperinfod contributors
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import ctypes
import ctypes.util
from ctypes import c_void_p, c_int, c_char_p, byref

c_ft_library = c_void_p
c_ft_face = c_void_p

guixPath = os.getenv ('GUIX_ENVIRONMENT', '/usr')
libPaths = [guixPath + '/lib', '/usr/lib64', '/usr/lib']
for p in libPaths:
	try:
		freetype = ctypes.CDLL(p + '/libfreetype.so')
		break
	except OSError:
		pass

class Context:
	__slots__ = ('_ctx', )

	_Init_FreeType = freetype.FT_Init_FreeType
	_Init_FreeType.argtypes = [c_ft_library]

	_New_Face = freetype.FT_New_Face
	_New_Face.argtypes = [c_ft_library, c_char_p, c_int, c_ft_face]

	def __init__ (self):
		self._ctx = c_void_p ()
		self._Init_FreeType (byref (self._ctx))

	@property
	def _as_parameter_ (self):
		return self._ctx

	def new_face (self, path, unknown):
		face = c_void_p ()
		self._New_Face (self, path.encode ('utf8'), unknown, byref (face))
		return face

