paperinfod
==========

*This software is probably not useful to you or the general public.*

paperinfod implements an ePaper information display for the Kindle Paperwhite 3.

.. image:: doc/example.png

Installation
------------

1. `Jailbreak your Kindle
   <https://www.mobileread.com/forums/showthread.php?t=345655>`_ and install
   `USBNet <https://www.mobileread.com/forums/showthread.php?t=225030>`_,
   as well as Python 3.
2. Clone https://codeberg.org/ldb/paperinfod.git.
3. On your PC run ``python3 setup.py assets`` to generate the icons. This
   depends on Inkscape.
4. Then run ``python3 setup.py bdist_wheel`` to generate a wheel in ``dist/``.
5. Copy the wheel to your Kindle and ``unzip`` it into
   ``/mnt/us/python3/lib/python3.9/site-packages``.
6. Create a ``config.ini`` on your Kindle, see ``doc/config.example.ini``.
7. Run ``/mnt/us/python3/bin/python3.9 -m paperinfod config.ini``.

For development you can also install the project into a virtualenv and
run it on your PC. It will render frames into ``display.png`` instead.

Implementation notes
--------------------

There’s no way to configure the displayed information right now. Edit
``src/paperinfod/__main__.py`` instead.

The *dwdforecast* data source cannot use data from MOSMIX S, because
the file downloaded is too big, i.e. does not fit into the Kindle’s
512MB RAM.

Related work
------------

This is not the first software using second hand Kindle hardware to
display information. Some other projects include:

* `Low-power Kindle dashboard <https://github.com/pascalw/kindle-dash>`_
* `Life Dashboard <https://github.com/davidhampgonsalves/life-dashboard>`_
* `Kindle Clock <https://github.com/mattzzw/kindle-clock>`_

